#include <vector>
#include <iostream>

int partition(std::vector<int>& arr, int low, int high)
{
    int i, piv;
    i = low - 1;
    piv = arr[high];
    int temp;

    for (int j = low; j < high; j++)
        if (arr[j] <= piv)
        {
            i++;
            temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }
    temp = arr[i+1];
    arr[i+1] = arr[high];
    arr[high] = temp;
    return i+1;
}

void quicksort(std::vector<int>& arr, int low, int high)
{
    int pin;
    if ((low < high) && arr.size() != 1)
    {
        pin = partition(arr, low, high);
        quicksort(arr, low, pin-1);
        quicksort(arr, pin+1, high);
    }
}

int main()
{
    // quicksort(arr, 0, arr.size()-1);
}
