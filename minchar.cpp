#include <vector>
using namespace std;

vector<char> solve(vector<string> words)
{
    vector<char> sol;
    map<char,int> mp;

    if (words.size() == 0)
        return {};

    for(int i=0;i<words.size();i++)
    {
        map<char,int> temp;
        for(char& c:words[i])
        {
            temp[c]++;
            if(temp[c]>mp[c])
                mp[c]=temp[c];
        }
    }

    for(auto& i:mp)
        for(auto j=0;j<i.second;j++)
            sol.push_back(i.first);
    return sol;
}
