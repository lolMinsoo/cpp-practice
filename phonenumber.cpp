class Solution
{
public:
    std::string mp[10] = { "", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};

    std::vector<std::string> letterCombinations(std::string digits)
    {
        if (digits.length() == 0)
            return {};

        std::vector<std::string> ans;
        dfs(digits, ans);
        return ans;
    }

    void dfs(std::string& str, std::vector<std::string>& sol)
    {
        std::queue<std::string> queue;
        queue.push("");

        while(!queue.empty())
        {
            std::string s = queue.front();
            queue.pop();

            if (s.length() == str.length())
                sol.push_back(s);
            else
                for (char& c : mp[(str[s.length()] - '0')])
                    queue.push(s+c);
        }
    }
}
