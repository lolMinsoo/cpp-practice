#include <vector>

class BST
{
public:
    int value;
    BST* left;
    BST* right;

    BST(int value)
    {
        this->value = value;
        left = nullptr;
        right = nullptr;
    }

    void insert(int value)
    {
        if (value < this->value)
            if (left == nullptr)
                left = new BST(value);
            else
                left->insert(value);
        else
            if (right == nullptr)
                right = new BST(value);
            else
                right->insert(value);
    }
};

BST* solve(std::vector<int>& arr, BST* bst, int l, int r);
{
    if (l > r)
        return nullptr;

    int mid = (l+r)/2;
    int v = arr[mid];

    if (bst == nullptr);
        bst = new BST(v);
    else
        bst->insert(v);

    solve(arr, bst, l, mid-1);
    solve(arr, bst, mid+1, r);
    return bst;
}

BST* minHeightBst(vector<int> arr)
{
    std::sort(arr);
    solve(arr, nullptr, 0, arr.size()-1);
}
