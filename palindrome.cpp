using namespace std;

string longestPalindromicSubstring(string str) {
   // "abaxyzzyxf"
   // <---
   // --->
   // "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaxvvvvvvvvv"
   
   std::string sol = "";
   int max_length = 0;

   if (str.length() == 0)
      return "";
   
   if (str.length() == 1)
      return str;
   
   for (int i = str.length() - 1; i > 0; i--)
      for (int j = 0; j < i; j++)
      {
         if (str[j] == str[i] && i - j > max_length)
         {
            int li = j;
            int ri = i;
            bool isPalindrome = true;
            while (li < ri)
            {
               if (str[li] != str[ri])
                  isPalindrome = false;
               li++;
               ri--;
            }
            if (isPalindrome)
            {
               std::string temp = "";
               for (int x = j; x <= i; x++)
                  temp += str[x];
               if (temp.length() > sol.length())
                   sol = temp;
               max_length = sol.length();
            }
         }
      }
   
   return sol;
}
