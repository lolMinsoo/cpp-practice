#include <vector>
#include <iostream>

void heapify(std::vector<int>& arr, int N, int i)
{
    // N = size of heap
    // i = Index

    int largest = i;
    int l = 2*i+1;
    int r = 2*i+2;

    // l, r must be < N to make sure we're on the heap.
    // In the case where left child is larger than the root.
    if (l < N && arr[l] > arr[largest])
        largest = l;

    // If right child is larger than the largest.
    if (r < N && arr[r] > arr[largest])
        largest = r;

    // Need to check to make sure the largest is the root, otherwise it's not max heap.
    if (largest != i)
    {
        std::swap(arr[i], arr[largest]);

        heapify(arr, N, largest);
    }
}

void heapsort(std::vector<int>& arr, int N)
{
    // Build heap
    for (int i = N/2 - 1; i >= 0; i--)
        heapify(arr, N, i);

    // Extract from heap.
    for (int i = N - 1; i > 0; i--)
    {
        std::swap(arr[0], arr[i]);
        heapify(arr, i, 0);
    }
}

void verify(std::vector<int>& arr)
{
    for (int i : arr)
        std::cout << i << " ";
    std::cout << std::endl;
}

int main()
{
    int N = 25;
    std::vector<int> arr;
    for (int i = 0; i < N; i++)
        arr.push_back(std::rand() % 100 + 1);

    verify(arr);
    heapsort(arr, N);
    verify(arr);
}
