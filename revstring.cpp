#include <vector>

std::string reverseWordsInString(std::string str)
{
    std::vector<std::string> m_str;
    std::string w = "";
    std::string sol = "";

    for(char& c : str)
        if(std::isspace(c))
        {
            m_str.push_back(w);
            w = "";
        }
        else
            w+=c;

   m_str.push_back(w);

   for(int i=m_str.size()-1; i>=0; i--)
       sol+=m_str[i]+" ";

   return sol.substr(0, sol.length()-1);
}
